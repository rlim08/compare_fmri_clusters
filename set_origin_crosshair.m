function image = set_origin_crosshair(image, origin, magnitude)
image(origin(1), :, :) = magnitude;
image(:, origin(2), :) = magnitude;
image(:, :, origin(3)) = magnitude;
end