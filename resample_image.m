function new_image = resample_image(component_image)
header = spm_vol(component_image);
dims = header.dim;
vox_size = abs(diag(header.mat(1:3, 1:3)))';
target_size = [79, 95, 68];
new_vox_size = [0,0,0];
if size(dims) ~= size(vox_size)
    disp('error');
    return;
end
for i=1:3
   new_vox_size(i) = dims(i)*vox_size(i)/target_size(i);
end

reslice_nii(component_image, [pwd filesep 'resampled_image.nii'], [2,2,2]);
hdr = spm_vol([pwd filesep 'resampled_image.nii']);

img = spm_read_vols(hdr);
hdr.dim = target_size;
new_image.header = hdr;

new_image.image = reshape(crop_image(img, size(img), target_size, hdr), [prod(hdr.dim), 1]);
end

function new_image = crop_image(image, original_size, new_size, hdr)
diff = original_size-new_size; %difference between old and new size
target_origin = [78, 112, 58]./2; % [39 56 29]
component_origin = abs(round(hdr.mat(1:3, 4)'./[2,2,2])); %[origin]
diff_origin = component_origin - target_origin;
image_size = original_size; % copy the original_size
start = [1,1,1];
for i = 1:3
   if diff_origin(i)>=0 && diff(i)-diff_origin(i)>=0 
        start(i) = diff_origin(i)+1;
   elseif diff_origin(i)<0 && diff(i)-diff_origin(i)>=0
        pad_size_begin = image_size; % copy the image size
        pad_size_begin(i) = -diff_origin(i); % padding must be the negative of the difference between the origin
        pad_zeros = zeros(pad_size_begin); % create padding matrix
        image = cat(i, pad_zeros, image); % pad image
   elseif diff_origin(i)>=0 && diff(i) - diff_origin(i)<0
        start(i) = diff_origin(i)+1; % since the diff_origin is larger, we have to shave off the lower indices to make the diff_origin = component origin
        pad_size_end = image_size;
        pad_size_end(i) = -(diff(i) - diff_origin(i)); % trust me, this is a thing
        pad_zeros = zeros(pad_size_end); % create padding matrix
        image = cat(i, image, pad_zeros); % pad image at the end this time
   elseif diff_origin(i)<0 && diff(i)-diff_origin(i)<0
        pad_size_begin = image_size;
        pad_size_begin(i) = -diff_origin(i);
        pad_zeros = zeros(pad_size_begin);
        image = cat(i, pad_zeros, image);
        pad_size_end = image_size;
        pad_size_end(i) = -(diff(i) - diff_origin(i));
        pad_zeros = zeros(pad_size_end);
        image = cat(i, image, pad_zeros);
   else
       error('something went wrong');
   end
   image_size = size(image);
end
new_image = image(start(1):start(1)+new_size(1)-1, start(2):start(2)+new_size(2)-1, start(3):start(3)+new_size(3)-1);
end