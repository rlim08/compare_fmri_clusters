function compare_template(path_to_mountdrive, component_image, threshold_list)
% function to pull in template images and compare them to a given component
% image. The assumptions that the function has are as follows:
% 1. The template images are on the path $Mountdrive/Templates/WoodwardNetworks
% 2. The ONLY '.nii' files that are in the path are the Templates
% 3. the component_image is a string that leads to a NIFTI file
% 4. there are 7 networks, numbered as 1. CE, 2. Lang, 3. IS, 4. VA, 5.
% Resp, 6. DMN, 7. PS(a)
% if any of these assumptions are broken, the function may not work
form = get(0, 'Format');
format('bank');
original_dir = pwd;
disp(['Current directory: ' original_dir]);
for i = 1:size(threshold_list, 2)
    component_image = image_exist(original_dir, component_image);
    if(exist([original_dir filesep num2str(threshold_list(i))], 'dir'))
        x = input(['Found existing directory: ' original_dir filesep num2str(threshold_list(i)) ' , do you want to overwrite? [y/n]: '],'s');
        if ~(strcmpi(x,'y'))
            continue;
        end
    else
        mkdir(num2str(threshold_list(i)));
    end
    cd(num2str(threshold_list(i)));
    try
        compare_template_runner(path_to_mountdrive, component_image, threshold_list(i));
    catch ME
        cd(original_dir);
        rethrow(ME);
    end
    cd(original_dir);
end
format(form);
end
function fullname = image_exist(original_dir, component_image)
if ~exist(component_image, 'file')
    error('image does not exist');
else
    [folder, filename, ext] = fileparts(component_image);
    if(isempty(folder))
        fullname = [original_dir filesep filename ext];
    else
        fullname = [folder filesep filename ext];
    end
end
end

function arr_cell = compare_template_runner(path_to_mountdrive, component_image, threshold)
if nargin <3
    threshold = 10;
end
cell_template_mapping = {'Cognitive Evaluation', 'Language', 'Inner Speech', ...
    'Visual Attention', 'High Visual Attention Cingulate', 'Right handed response', 'Two Handed response', ...
    'Default Mode Network', 'Auditory Primary Sensory', 'Visual Primary Sensory'}';
to_compare = resample_image(component_image);
templates = pull_templates(path_to_mountdrive);
exemplars = pull_exemplars(path_to_mountdrive);
to_compare = to_compare.image;

top = 100-threshold; pos = to_compare; pos(pos<0)=0; neg = to_compare; neg(neg>0)=0;
percentile = prctile(abs(to_compare), top);
if percentile ~= 0
    pos = abs(pos)>= percentile; neg = abs(neg) >= percentile;
else
    pos = abs(pos)>percentile; neg = abs(neg)>percentile;
end
arr_cell = comparison(pos, templates, exemplars, cell_template_mapping, 'positive');
arr_cell = [arr_cell; comparison(neg, templates, exemplars, cell_template_mapping, 'negative')];
end
function arrs = comparison(to_compare, templates, exemplars, cell_template_mapping, pos_neg)
correls=comp_func(to_compare, templates, exemplars);
[~, max_ind_template] = max(abs([correls{:, 1}]));
[~, max_ind_comparison] = max(abs([correls{:, 2}]));
[~, max_ind_union] = max(abs([correls{:, 3}]));
[~, max_ind_ex] = max(abs([correls{:, 4}]));
if any(to_compare)
    disp([pos_neg ' voxels (' num2str(sum(to_compare)) '/' num2str(numel(to_compare)) ')' ]);
    table = cell2table([cell_template_mapping correls] , 'VariableNames', {'Network', 'intersection_template', 'intersection_to_be_classfied', 'intersection_union', 'Exemplar_correlation'});
    disp(table)
    writetable(table,[pos_neg '_results.txt'], 'Delimiter', '\t');
    disp(['most correlated network template intersection_template: ', cell_template_mapping{max_ind_template}]);
    disp(['most correlated network template intersection_to_be_classified: ', cell_template_mapping{max_ind_comparison}]);
    disp(['most correlated network template intersection_union: ', cell_template_mapping{max_ind_union}]);
    disp(['most correlated network exemplar: ', cell_template_mapping{max_ind_ex}]);
    fid = fopen([pos_neg '_results.txt'], 'at');
    fprintf(fid, 'number of voxels: %d \n', sum(to_compare));
    fprintf(fid, 'most correlated network template intersection_template: %s \n', cell_template_mapping{max_ind_template});
    fprintf(fid,'most correlated network template intersection_to_be_classified: %s \n', cell_template_mapping{max_ind_comparison});
    fprintf(fid, 'most correlated network template intersection_union: %s \n', cell_template_mapping{max_ind_union});
    fprintf(fid, ['most correlated network exemplar: ', cell_template_mapping{max_ind_ex}]);
    fclose(fid);
else
    disp(['No ' pos_neg ' voxels found']);
    fid = fopen([pos_neg '_results.txt'], 'at');
    fprintf(fid, 'No voxels found');
    fclose(fid);
end
arrs = return_arrays(to_compare, templates, exemplars, cell_template_mapping, pos_neg(1));


end


function arr_cell = return_arrays(image_to_compare, templates, exemplars, names, pos_neg)
arr_cell = cell(size(templates,1), nargin-1);

for i = 1:size(templates, 1)
    arr_cell{i,1} = strrep(names{i}, ' ', '_');
    arr_cell{i,2} = templates{i}.image;
    arr_cell{i,3} = exemplars{i}.image;
    arr_cell{i,4} = image_to_compare;
    arr_cell{i,5} = image_to_compare;
    create_nii_file(templates{i}, image_to_compare, [num2str(i) '. ' pos_neg '_' strrep(names{i}, ' ', '_')]);
end
arr_cell = cell2struct(arr_cell, {'name', 'templates', 'exemplars', 'comparison_image', 'comparison_image_thresholded'},2);

end

function create_nii_file(template, to_compare, name)
template_image = reshape(logical(template.image)*2, template.header.dim);
sum_image = reshape(to_compare, template.header.dim)+template_image;
to_write = template;
to_write.image = sum_image;
to_write.header.fname = [pwd filesep name '_image.nii'];
spm_write_vol(to_write.header, to_write.image);

end

function new_image = expand_image(image_3d)
nx = 79; ny = 95; nz = 68;
image_3d_new = resize(image_3d, [nx, ny, nz]);
new_image = reshape(image_3d_new, [prod([nx, ny, nz]), 1]);
end

function templates = pull_templates(path_to_mountdrive)
template_file_type = '.nii';
path_to_templates = [path_to_mountdrive filesep 'Templates', filesep, 'WoodwardNetworks' filesep ];
template_files = dir([path_to_templates '*' template_file_type]);
template_names = {template_files.name}';

templates = cell(size(template_names,1),1); % initialize cell array to hold templates
for i=1:size(template_names)
    if ~isempty(strmatch([num2str(i) '.'], template_names))
        header = spm_vol([path_to_templates ...
            template_names{strmatch([num2str(i) '.'], template_names)}]);
        image = spm_read_vols(header);
        templates{i, 1}.header = header;
        
        templates{i, 1}.image = reshape(image, [prod(header.dim), 1]);
    end
end
templates =  templates(~cellfun('isempty',templates));
end

function exemplars = pull_exemplars(path_to_mountdrive)
exemplar_file_type = '.img';
path_to_exemplars = [path_to_mountdrive filesep 'Templates', filesep, 'WoodwardNetworks' filesep 'Exemplars' filesep];
exemplar_files = dir([path_to_exemplars '*' exemplar_file_type]);
exemplar_names = {exemplar_files.name}';
exemplars = cell(size(exemplar_names,1),1); % initialize cell array to hold templates
for i=1:size(exemplar_names)
    if ~isempty(strmatch([num2str(i) '.'], exemplar_names))
        header = spm_vol([path_to_exemplars ...
            exemplar_names{strmatch([num2str(i) '.'], exemplar_names)}]);
        image = spm_read_vols(header);
        exemplars{i, 1}.header = header;
        exemplars{i, 1}.image = reshape(image, [prod(header.dim),1]);
        exemplars{i, 1}.image = expand_image(reshape(exemplars{i, 1}.image, exemplars{i,1}.header.dim));
    end
end
exemplars = exemplars(~cellfun('isempty',exemplars));
end

