function correls = comp_func(to_compare, templates, exemplars)
correls = cell(size(templates, 1), 2);
for i=1:size(templates)
    correls{i, 1} = round(jaccard_score_template(to_compare, logical(templates{i, 1}.image))*10000)/100;
    correls{i, 2} = round(jaccard_score_comparison(to_compare, logical(templates{i, 1}.image))*10000)/100;
    correls{i, 3} = round(jaccard_score_union(to_compare, logical(templates{i, 1}.image))*10000)/100;
    correls{i, 4} = round(correlate_array(to_compare, exemplars{i, 1}.image)*100)/100;
end
end

function correl = correlate_array(to_compare, template)
    correl = corr(to_compare, template);
end

function jaccard = jaccard_score_union(to_compare, template)
    inter_image = to_compare & template;
    union_image = to_compare | template;
    
    jaccard = sum(inter_image)/sum(union_image);
end

function jaccard = jaccard_score_comparison(to_compare, template)
    inter_image = to_compare & template;    
    jaccard = sum(inter_image)/sum(to_compare);
end

function jaccard = jaccard_score_template(to_compare, template)
    inter_image = to_compare & template;
    jaccard = sum(inter_image)/ sum(template);

end
