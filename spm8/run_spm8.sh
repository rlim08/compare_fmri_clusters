#!/bin/sh
# script for execution of deployed SPM8
#
# Sets up the MCR environment for the current $ARCH and launch SPM8.
#
if [ "x$1" = "x" ]; then
  echo Usage:
  echo    $0 \<deployedMCRroot\> args
else
  # or set MCRROOT here (instead of in the command line) and comment out the next line
  MCRROOT=$1 ; shift 1
  MACAPP="" ;
  PLATFORM=`uname` ;
  if [ "$PLATFORM" = "Linux" ]; then
    HARDWARE=`uname -m` ;
    if [ "$HARDWARE" = "i386" -o "$HARDWARE" = "i686" ]; then
      MWE_ARCH="glnx86" ;
      MWE_JRE="i386" ;
    elif [ "$HARDWARE" = "x86_64" ]; then
      MWE_ARCH="glnxa64" ;
      MWE_JRE="amd64" ;
    else
      echo "Unknown platform ${PLATFORM}-${HARDWARE}" ;
      return;
    fi
  elif [ "$PLATFORM" = "Darwin" ]; then
    MWE_ARCH="maci64" ;
    MWE_JRE="amd64" ;
    MACAPP="spm8.app/Contents/MacOS/"
  else
    echo "Unknown platform ${PLATFORM}" ;
    return;
  fi
  if [ "$MWE_ARCH" = "maci64" ]; then
    DYLD_LIBRARY_PATH=.:${MCRROOT}/runtime/${MWE_ARCH} ;
    DYLD_LIBRARY_PATH=${DYLD_LIBRARY_PATH}:${MCRROOT}/bin/${MWE_ARCH} ;
    DYLD_LIBRARY_PATH=${DYLD_LIBRARY_PATH}:${MCRROOT}/sys/os/${MWE_ARCH} ;
    DYLD_LIBRARY_PATH=${DYLD_LIBRARY_PATH}:/System/Library/Frameworks/JavaVM.framework/JavaVM:/System/Library/Frameworks/JavaVM.framework/Libraries;
    export DYLD_LIBRARY_PATH;
  else
    LD_LIBRARY_PATH=.:${MCRROOT}/runtime/${MWE_ARCH} ;
    LD_LIBRARY_PATH=${LD_LIBRARY_PATH}:${MCRROOT}/bin/${MWE_ARCH} ;
    LD_LIBRARY_PATH=${LD_LIBRARY_PATH}:${MCRROOT}/sys/os/${MWE_ARCH} ;
    MCRJRE=${MCRROOT}/sys/java/jre/${MWE_ARCH}/jre/lib/${MWE_JRE} ;
    LD_LIBRARY_PATH=${LD_LIBRARY_PATH}:${MCRJRE}/native_threads ; 
    LD_LIBRARY_PATH=${LD_LIBRARY_PATH}:${MCRJRE}/server ;
    LD_LIBRARY_PATH=${LD_LIBRARY_PATH}:${MCRJRE}/client ;
    LD_LIBRARY_PATH=${LD_LIBRARY_PATH}:${MCRJRE} ; 
    export LD_LIBRARY_PATH;
  fi
  XAPPLRESDIR=${MCRROOT}/X11/app-defaults ;
  export XAPPLRESDIR;
  if [ "$XUSERFILESEARCHPATH" != "" ]; then
    XUSERFILESEARCHPATH=$XUSERFILESEARCHPATH:$XAPPLRESDIR/%N
    export XUSERFILESEARCHPATH
  fi
  `dirname $0`/${MACAPP}spm8_${MWE_ARCH} $*
fi
exit
